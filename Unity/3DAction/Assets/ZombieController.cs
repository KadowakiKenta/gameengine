﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    //直進スピードの除算
    //値が大きくなるほど、スピードが下がる
    private const float divSpeed = 400.0f;

    //メインカメラオブジェクト
    private GameObject mainCameraObject_;

    //ゾンビのY軸初期回転
    private float zombieRotateInitY_;

    void Start()
    {
        //ゲームオブジェクトを取得
        mainCameraObject_ = Camera.main.gameObject;

        //初期回転を代入
        zombieRotateInitY_ = this.transform.localEulerAngles.y;
    }
    
    void Update()
    {
        //進む処理
        Move();

        //メインカメラへ回転させる処理
        MainCameraToAngle();
    }

    //メインカメラへ回転させる処理
    private void MainCameraToAngle()
    {
        //ゾンビの三次元ベクトルを取得(yは0)
        Vector3 zombiePosInit = this.transform.position;
        zombiePosInit.y = 0;
        zombiePosInit = zombiePosInit.normalized;

        //z軸が1の三次元ベクトルを作成
        Vector3 vecZ = new Vector3(0.0f, 0.0f, 1.0f);

        //内積に変換
        float dot = Vector3.Dot(zombiePosInit, vecZ);

        //acosで角度を求める
        float angle = Mathf.Acos(dot);

        //ラジアンから度に変更
        angle = angle * Mathf.Rad2Deg;

        //ゾンビが常にカメラの方へ向くようにする
        Vector3 rotate = this.transform.localEulerAngles;
        rotate.y = angle + zombieRotateInitY_;
        if (this.transform.position.x < 0.0f)
        {
            rotate.y = -angle + zombieRotateInitY_;
        }
        this.transform.localEulerAngles = rotate;
    }

    //進む処理
    private void Move()
    {
        //メインカメラまでの距離を取得
        Vector3 moveCamera = this.transform.position - mainCameraObject_.transform.position;

        //yを0にする
        moveCamera.y = 0;

        //直進！
        this.transform.position -= moveCamera / divSpeed;
    }

    //当たった時の処理
    private void OnCollisionEnter(Collision collision)
    {
        //石が当たった
        if (collision.gameObject.tag == "Stone")
        {
            //自分を消す
            Destroy(gameObject);
        }
    }
}

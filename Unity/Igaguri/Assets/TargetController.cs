﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    float count;
    float increment;
    int zPos;
    

    // Start is called before the first frame update
    void Start()
    {
        count = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        count += increment;
        float xPos = Mathf.Sin(count) * 20;

        transform.position = new Vector3(xPos, 0, zPos);
    }

    public void SetZPos(int zPosition)
    {
        zPos = zPosition;
    }

    public void SetIncrement(float inc)
    {
        increment = inc;
    }
}

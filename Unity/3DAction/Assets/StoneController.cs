﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneController : MonoBehaviour
{
    //石を投げる
    //引数：dir 投げる方向
    public void Shoot(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(dir);
    }

    //当たった時の処理
    private void OnCollisionEnter(Collision collision)
    {
        //床かゾンビに当たった
        if ( (collision.gameObject.tag == "Terrain") || (collision.gameObject.tag == "Zombie") )
        {
            //自分を消す
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    GameObject car;
    GameObject flag;
    GameObject distance;

    // Start is called before the first frame update
    void Start()
    {
        this.car = GameObject.Find("car");
        if(car == null)
        {
            Debug.Log("carError");
        }
        this.flag = GameObject.Find("flag");
        if (flag == null)
        {
            Debug.Log("flagError");
        }
        this.distance = GameObject.Find("Distance");
        if (distance == null)
        {
            Debug.Log("distanceError");
        }
    }

    // Update is called once per frame
    void Update()
    {
        float length = this.flag.transform.position.x -
            this.car.transform.position.x;
        if (length >= 0)
        {
            this.distance.GetComponent<Text>().text =
            "ゴールまで" + length.ToString("F2") + "m";
        }
        else
        {
            this.distance.GetComponent<Text>().text = "ゲームオーバー";
        }

    }
}

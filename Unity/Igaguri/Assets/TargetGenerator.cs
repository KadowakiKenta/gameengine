﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetGenerator : MonoBehaviour
{
    public GameObject targetPrefab;
    int Generation = 5; //生成する数
    //float collisionRadius = 3.8f;

    // Start is called before the first frame update
    void Start()
    {
        int[] zPos = new int[Generation];
        int putZPos = 0;
        for(int i = 0; i < Generation; i++)
        {
            zPos[i] = putZPos;

            GameObject go = Instantiate(targetPrefab) as GameObject;
            go.name = targetPrefab.name;
            go.transform.position = new Vector3(0, 0, zPos[i]);
            go.GetComponent<TargetController>().SetZPos(zPos[i]);

            float randXPos = Random.Range(0.005f, 0.01f);
            go.GetComponent<TargetController>().SetIncrement(randXPos);

            putZPos += 5;
        }

        /*
        int[] xPos = new int[Generation];

        for (int i = 0; i < Generation; i++)
        {
            while(true)
            {
                int x = Random.Range(-30, 31);

                bool collisionFlag = false;
                for (int j = 0; j < i; j++)
                {
                    if( (x - xPos[j] <= collisionRadius) &&
                        (x - xPos[j] >= -collisionRadius) )
                    {
                        collisionFlag = true;
                        continue;
                    }
                }

                if(!collisionFlag)
                {
                    GameObject go = Instantiate(targetPrefab) as GameObject;
                    go.name = targetPrefab.name;
                    go.transform.position = new Vector3(x, 0, 10);
                    xPos[i] = x;
                    break;
                }
            }
        }
        */
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    //ゲームオーバーまでの距離
    private const float gameOverPos = 1.0f;

    //ソンビオブジェクトの数
    private GameObject[] zombieObject_;

    //石オブジェクトの数
    private GameObject[] stoneObject_;

    //クリアテキスト
    private GameObject resultText_;

    //ゲームオーバーのフラグ
    public bool gameOverFlag_ { set; get; } = false;

    void Start()
    {
        //クリアテキストのゲームオブジェクトを取得
        resultText_ = GameObject.Find("ResultText");
    }

    void Update()
    {
        //ゾンビオブジェクトのタグを調べる
        zombieObject_ = GameObject.FindGameObjectsWithTag("Zombie");

        //ゾンビがいなくなった
        if(zombieObject_.Length == 0)
        {
            //クリア！
            resultText_.GetComponent<Text>().text =
                "Clear!";
        }

        //誰かのゾンビが一定位置まで来た
        for(int i = 0; i < zombieObject_.Length; i++)
        {
            if(zombieObject_[i].transform.position.z < gameOverPos)
            {
                //石オブジェクトのタグを調べる
                stoneObject_ = GameObject.FindGameObjectsWithTag("Stone");

                //石を全て消す
                for(int j = 0; j < stoneObject_.Length; j++)
                {
                    Destroy(stoneObject_[j].gameObject);
                }

                //ゲームオーバー！
                resultText_.GetComponent<Text>().text =
                    "Game Over!";

                //ゲームオーバーフラグをオンにする
                gameOverFlag_ = true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageAction : MonoBehaviour
{
    public Sprite[] images;
    public GameObject imageLeft;

    string[] message =
    {
        "こんに<color=red>ちは！！！！</color>",
        "LIC0",
        "どうも皆さん",
        "LIC1",
        "カサックスです",
        "エクストリームバーサス大学動物園科卒業です。"
    };

    int scenario = -1;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //エンターキー押したら
        if(Input.GetKeyDown(KeyCode.Return))
        {
            scenario++;

            if (message[scenario].Substring(0, 3) == "LIC")
            {
                int id = int.Parse(message[scenario].Substring(3));

                imageLeft.GetComponent<Image>().sprite = images[id];
            }
            else
            {
                GetComponent<Text>().text = message[scenario];
            }
        }
    }
}

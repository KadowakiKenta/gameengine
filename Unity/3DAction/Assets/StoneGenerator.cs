﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneGenerator : MonoBehaviour
{
    //投げる強さ
    private const float power = 1000.0f;

    //石のプレファブ
    public GameObject stonePrefab_;

    //監督
    private GameObject gameDirector_;

    void Start()
    {
        //監督を探す
        gameDirector_ = GameObject.Find("GameDirector");
    }

    void Update()
    {
        //ゲームオーバーか判定
        bool gameOverFlag = gameDirector_.GetComponent<GameDirector>().gameOverFlag_;

        //左クリック・ゲームオーバーではない
        if( (Input.GetMouseButtonDown(0)) && (!gameOverFlag) )
        {
            //石のインスタンスを作成
            GameObject stone = Instantiate(stonePrefab_) as GameObject;

            //カメラからマウスのクリック場所までのレイを取得
            Ray cameraToMouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            //レイの三次元ベクトルを取得
            Vector3 worldDir = cameraToMouseRay.direction;

            //石を投げる！
            stone.GetComponent<StoneController>().Shoot(worldDir.normalized * power);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class ScoreDirector : MonoBehaviour
{
    int point = 0;
    GameObject score;

    // Start is called before the first frame update
    void Start()
    {
        score = GameObject.Find("Score");
    }

    // Update is called once per frame
    void Update()
    {
        this.score.GetComponent<Text>().text =
            "Score：" + point.ToString();
    }

    public void PlusPoint()
    {
        point += 10;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgaguriController : MonoBehaviour
{
    public void Shoot(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(dir);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "targetPrefab")
        {
            GameObject scoreDirector = GameObject.Find("ScoreDirector");
            scoreDirector.GetComponent<ScoreDirector>().PlusPoint();
            Destroy(other.gameObject);
            GetComponent<Rigidbody>().isKinematic = true;
        }

        GetComponent<ParticleSystem>().Play();
    }

    // Start is called before the first frame update
    void Start()
    {
        //Shoot(new Vector3(0, 200, 2000));
    }

    void Update()
    {
        GetComponent<Rigidbody>().isKinematic = false;
    }
}
